FROM alpine:edge
RUN apk add  --repository http://dl-cdn.alpinelinux.org/alpine/edge/community dbus-x11 \
            libpurple-xmpp purple-carbons purple-lurch purple-xmpp-http-upload pidgin-dev \
            build-base cyrus-sasl meson ninja gtk+3.0-dev libhandy-dev evolution-dev \
            evolution-data-server-dev libgee-dev folks-dev feedbackd-dev appstream-glib \
            desktop-file-utils libhandy1-dev libphonenumber-dev git gdb olm olm-dev \
            purple-mm-sms modemmanager-dev gspell gspell-dev glib-dev json-glib-dev \
            zlib-dev terminus-font\
            && fc-cache -fv
RUN apk add -X http://dl-cdn.alpinelinux.org/alpine/edge/testing  purple-facebook
RUN wget https://github.com/EionRobb/skype4pidgin/archive/1.7.tar.gz && tar xf 1.7.tar.gz && \
    cd /skype4pidgin-1.7/skypeweb && make && make install && cd / && rm -rf skype4pidgin-1.7 1.7.tar.gz

RUN wget https://github.com/dylex/slack-libpurple/archive/6fe5c4c6cc46ce8662569e1c373d9f7c457bf23a.tar.gz && \
    tar xf 6fe5c4c6cc46ce8662569e1c373d9f7c457bf23a.tar.gz && \
    cd slack-libpurple-6fe5c4c6cc46ce8662569e1c373d9f7c457bf23a/ && make && make install && cd / && \
    rm -rf slack-libpurple-6fe5c4c6cc46ce8662569e1c373d9f7c457bf23a/ 6fe5c4c6cc46ce8662569e1c373d9f7c457bf23a.tar.gz

COPY .git chatty/.git
WORKDIR chatty/
RUN git checkout .
RUN abuild-meson . output --buildtype=debug
RUN meson compile -C output
RUN meson install --no-rebuild -C output
COPY purple/libfacebook.so /usr/lib/purple-2/libfacebook.so
COPY purple/new-slack /root/.purple
#RUN mv /root/.purple/chatty/db/chatty-history-old.db /root/.purple/chatty/db/chatty-history.db
CMD gdb --args chatty --debug --verbose

#docker run --rm -it -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix:ro -v $XAUTHORITY:/root/.Xauthority --security-opt label=type:container_runtime_t --net=host chatty
